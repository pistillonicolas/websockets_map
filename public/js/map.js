const Toast = Swal.mixin({
    toast: true,
    position: "top-end",
    showConfirmButton: false,
    timer: 6000,
    timerProgressBar: true,
    didOpen: (toast) => {
        toast.addEventListener("mouseenter", Swal.stopTimer);
        toast.addEventListener("mouseleave", Swal.resumeTimer);
    },
});

// Enable real-time markers update
Echo.channel("updateMap").listen(".updatePositionMap", (positionObj) => {
    
    console.log(positionObj);

    addMarkerToGroup(
        group,
        {
            lat: positionObj.position.coords.Latitude,
            lng: positionObj.position.coords.Longitude,
        },
        `
        <div style="min-width:300px">
            <h2 class="text-gray-800 font-semibold">${positionObj.position.address.Label}</h2>
            <p class="text-gray-500">${positionObj.update_at}</p>
        </div>
        `
    );

    Toast.fire({
        icon: "info",
        title: `Nueva dirección recibida`,
    });

    map.setCenter({
        lat: positionObj.position.coords.Latitude,
        lng: positionObj.position.coords.Longitude,
    });

    map.setZoom(15);
});

var platform = new H.service.Platform({
    apikey: document.getElementById("map").dataset.here,
});

var defaultLayers = platform.createDefaultLayers();

var map = new H.Map(
    document.getElementById("map"),
    defaultLayers.vector.normal.map,
    {
        center: { lat: 50, lng: 5 },
        zoom: 4,
        pixelRatio: window.devicePixelRatio || 1,
    }
);

var group = new H.map.Group();

window.addEventListener("resize", () => map.getViewPort().resize());

var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

var ui = H.ui.UI.createDefault(map, defaultLayers);

function addMarkerToGroup(group, coordinate, html) {
    var marker = new H.map.Marker(coordinate);

    marker.setData(html);
    group.addObject(marker);
}

// Show the info bubbles on "tap" in markers
function addInfoBubbles(map) {

    map.addObject(group);

    group.addEventListener(
        "tap",
        function (evt) {
            var bubble = new H.ui.InfoBubble(evt.target.getGeometry(), {
                content: evt.target.getData(),
            });

            ui.addBubble(bubble);
        },
        false
    );
}

window.onload = function () {
    addInfoBubbles(map);
};