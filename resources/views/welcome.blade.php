@extends('layouts.dashboard')

@section('page_content')

    <div id="map" data-here="{{ env('HERE_API_KEY') }}" style="width:100%;height:470px"></div>

@endsection

@section('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/map.js') }}"></script>
@endsection