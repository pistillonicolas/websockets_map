<?php

namespace App\Console\Commands;

use App\Events\UpdatePosition;
use Illuminate\Console\Command;
use Thiagoprz\HereGeocoder\HereGeocoder;
use stdClass;

class TestMapPositions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test:position';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test de eventos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $geo = new HereGeocoder();
        $street = $geo->geocode('Zinny 1879 Buenos Aires');

        if (!empty($street->Response->View)) {

            $map_data = new stdClass();

            $map_data->coords =  $street->Response->View[0]->Result[0]->Location->DisplayPosition;
            $map_data->address =  $street->Response->View[0]->Result[0]->Location->Address;

            event(new UpdatePosition($map_data));

        }
    }
}
